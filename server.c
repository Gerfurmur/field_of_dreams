#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/poll.h>
#include <sys/shm.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <unistd.h>
#include <signal.h>
#include <resolv.h>
#include <time.h>
#include <errno.h>
#include <stdarg.h>
#include <arpa/inet.h>
#include <semaphore.h>

#define CHILD_NEED_TERMINATE 1
#define CHILD_NEED_WORK 0
#define SEND_NOTIFICATION 2 // изначальное состояние, ждем сообщения start{pwd}
#define CREATE_GAME 3 		// после прихода сбщ start{} переходим в это состояние (в это состояние переводит GENERATOR, в мастере обновляем текущее кол-во игроков) 
#define START_GAME 4 		// после прихода верного join{} отпр данное знч мастеру
#define CLIENT1_TURN 5 		// после появления второго игрока, переходим в это состояние (переход должен выполнить EXECUTOR, в мастере обновляем текущее кол-во игроков)
							// если мастеру приходит данное состояние, значит сбщ пользователя прошло проверку и требуется запустить EXECUTOR
#define CLIENT2_TURN 6 		// после неудачного хода первого, переходим в это состояние, аналогично после неудачи второго в предыдущее
#define GAME_END 7     		// расформирование игры
#define WINNER1 8	   		// победитель - игрок 1
#define WINNER2 9	   		// победитель - игрок 2

#define NEED_EXV_GUESS 11	// если ARBITRATOR прислал данное значение, создаем executorGuess
#define NEED_EXV_ANSWER 12	// если ARBITRATOR прислал данное значение, создаем executorAnswer
#define WRONG_TURN 10 		// если ARBITRATOR прислал данное значение в сбщ, то пишем клиенту подождать своего хода
// в shared memory хранить текущее состояние, загаднное слово, текущее состояние слова, очки игроков., сокеты игроков


#define MSGSZ 256

#define MAX_PLAYERS 2
#define MAX_POINT 10

char * cfg_file; // файл конфига
int PORT_CON; // определяем порт который будет прослушивать наш эхо сервер (из файла конфига)
char namlog[128]; // определяем файл, куда записываются логи (из файла конфига)

int client_socket_fd[10] = {0}, player_sockets[2] = {0}, player_points[2] = {0}; // идентификатор клиентских сокетов, идентификаторы игроков
int cur_socket; // текущий сокет для случая отключения клиента
unsigned int cur_points = 0;
int blocked_player = 0; // если игрок попытался угадать слово и не смог, то заносим его в список заблокированных.
						// Если игрок попытался, а значение уже есть, то игра расформировывается

int socket_fd; // объявляем идентификатор сокета нашего сервера
char host[20];
struct sockaddr_in server, address;// структура sockaddr_in для нашего сервера
socklen_t size = sizeof(address);// размер структуры (пока неизвестен)

struct message{
    long mtype;        // Тип сообщения
    char mesg[MSGSZ]; // Само сообщение, длиной MSGSZ.
};

struct message msg_h;

struct gameStruct{
	sem_t * sem_lock;
	int state;
	char word[256];
	char cur_word[256];
};

struct gameStruct * game_h;
char * attached_data;

int shmid; // идентификатор разделяемой памяти
int msqid; // идентификатор очереди сообщений
sem_t semaphore;

// ОБЪЯВЛЕНИЕ ПЕРЕМЕННЫХ
char * log_prefix; // префикс логов для удобства читаемости логов
pid_t child_pid = -1; // пид последнего воркера
int status, need_terminate = 0, clients_amount = 0, cur_players = 0;
char sockbuff[256]; // наш буфер обмена информацией с клиентом
time_t now;
struct tm *ptr;
char tbuf[80], val[20], pwd[20] = {0};
char * PID_FILE = "field_of_dreams.pid";

// ОБЪЯВЛЕНИЕ ФУНКЦИЙ
int monitorProc();
char * getTime();
int disconnect(int index);
int connectSocket();
int createLogFile();
int loadConfig(char * cfg_file);
int monitorProc();
void setPidFile(char* Filename);
int sendNotification(int socket_fd);
int initWorkThread();
unsigned int generateRandomByte();
int arbitrator(char * client_msg);
int executeAnswer(char * word, int client_socket);
int executeGuess(char * letter, int client_socket);
int generator(int client_socket);
int gameMaster();
void writeLog(char * fmt, ...);


int main(int argc, char** argv) {
    pid_t pid;
    
    if (argc != 2) {
        printf("Usage: ./server filename.cfg\n");
        return -1;
    }
    
    pid = fork();

    if (pid == -1) {
        printf("Error: Start Daemon failed (%s)\n", strerror(errno));
        return -1;
    }
    else if (!pid) {
		cfg_file = argv[1];
		status = loadConfig(cfg_file);
		
		if (-1 == status) {// если произошла ошибка загрузки конфига
			printf("Error: Load config failed\n");
			return -1;
		}
		
        // разрешаем выставлять все биты прав на создаваемые файлы,
        // иначе у нас могут быть проблемы с правами доступа
        umask(0);
        
        // создаём новый сеанс, чтобы не зависеть от родителя
        setsid();

        // закрываем дискрипторы ввода/вывода/ошибок, так как нам они больше не понадобятся
        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
        
        // Данная функция будет осуществлять слежение за процессом
        status = monitorProc();
        
        return status;
    }

    return 0;
}


// ----------------------------------- PARSER -----------------------------------
char * parse(char * str, char * param) {
	int str_len = strlen(str), param_len = strlen(param), i = 0, j = 0;
	while (i < str_len && strncmp(&str[i], param, param_len))
		++i;
	
	if (i >= str_len)
		return "\0";
	
	char *ret;
	ret = (char*)malloc(20);
	bzero(ret, 20);
	bzero(val, 20);
	
	i += param_len + 1;
	while (*(str + i) != ' ' && *(str + i) != '\n' &&  *(str + i) != '\0' && j < 19) {
		val[j] = str[i];
		++i;
		++j;
	}
	
	ret = val;
	
	return (ret);
}


// ----------------------------------- CONFIG SOCKET TIME -----------------------------------
char * getTime() {//--функция определения времени в нужном формате
	char *ret;
	ret = (char*)malloc(80);
	bzero(ret, 80);
	time(&now);
	ptr = localtime(&now);
	strftime(tbuf, 80, "%d.%m.%Y_%H:%M", ptr);
	ret = tbuf;
	
	return (ret);
}


int socketInNonblock(int socket_fd) {
	if (fcntl(socket_fd, F_SETFL, O_NDELAY) != 0) {
		writeLog("[ERROR] %s (%s): fcntl socket to nonblock mode: %s", log_prefix, getTime(), strerror(errno));
		return -1;
	}
	
	return 0;
}


int connectSocket() {
	if ((socket_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) { //--создаем сокет сервера в режиме TCP
		writeLog("[ERROR] %s (%s): socket create: %s", log_prefix, getTime(), strerror(errno));
		return -1;
	}
	
	server.sin_family = AF_INET; //--говорим что сокет принадлежит к семейству интернет
	server.sin_addr.s_addr = INADDR_ANY; //--наш серверный сокет принимает запросы от любых машин с любым IP-адресом
	server.sin_port = htons(PORT_CON); //--и прослушивает порт PORT_CON
	int opt = 1;
	
	if (setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) == -1) {
        writeLog("[ERROR] %s (%s): setsockopt: %s", log_prefix, getTime(), strerror(errno));
		return -1;
    }
	
	if (bind(socket_fd, (struct sockaddr *)&server, sizeof(server)) == -1) {//--функция bind спрашивает у операционной системы,может ли программа завладеть портом с указанным номером
		writeLog("[ERROR] %s (%s): socket bind: %s", log_prefix, getTime(), strerror(errno)); //--ну если не может,то выдается предупреждение 
		return -1;
	}

	if (listen(socket_fd, 10) == -1) { //--перевод сокета сервера в режим прослушивания с очередью в 20 позиций
        writeLog("[ERROR] %s (%s): listen: %s", log_prefix, getTime(), strerror(errno));
		return -1;
	}
	
	return socketInNonblock(socket_fd);
}


int createLogFile() {
	if (open(namlog, O_CREAT | O_TRUNC | O_WRONLY, S_IRWXU | S_IRWXG) == -1) {
		perror("create logfile:");
		return -1;
	}
	
	return 0;
}


void setPidFile(char* Filename) {
    FILE* f;

    f = fopen(Filename, "w+");
    if (f) {
        fprintf(f, "%u\n", getpid());
        fclose(f);
    }
}
	

int loadConfig(char * cfg_file) { // создает файл
	int fd;
	char buf[256] = {0};
	
	if ((fd = open(cfg_file, O_RDONLY)) != -1) {
		struct stat fileStat;
		fstat(fd, &fileStat);
		
		read(fd, &buf[0], fileStat.st_size);
		close(fd);
		
		PORT_CON = atoi(parse(buf, "port"));
		
		char * logFileName = parse(buf, "log");
		strncpy(namlog, logFileName, strlen(logFileName));
		
		strcat(namlog, getTime());
		strcat(namlog, ".log");
	
		if (createLogFile() == -1)
			return -1;
		
		return 0;
	}
	
	return -1;
}


int reloadConfig(char * cfg_file) {
	close(socket_fd);
	unlink(namlog);
	
	for (int i = 0; i < clients_amount; ++i)
		close(client_socket_fd[i]);
	
	if (-1 == loadConfig(cfg_file))
		return -1;
	if (connectSocket() == -1)
		return -1;
	return 0;
}


// ----------------------------------- LOGGING -----------------------------------
char * convert(unsigned int num, int base) { 
    static char Representation[]= "0123456789ABCDEF";
    static char buffer[50]; 
    char *ptr; 

    ptr = &buffer[49]; 
    *ptr = '\0'; 

    do 
    { 
        *--ptr = Representation[num%base]; 
        num /= base; 
    }while(num != 0); 

    return(ptr); 
}


void writeLog(char * fmt, ...) {
	FILE * logfile = fopen(namlog, "a+");
    char *traverse; 
    int i; 
    char *s; 

    //Module 1: Initializing Myprintf's arguments 
    va_list arg; 
    va_start(arg, fmt);
	

    for(traverse = fmt; *traverse != '\0'; ++traverse) {
        if (*traverse != '%') {
            fputc(*traverse, logfile);
            continue;
        }

        traverse++;

        //Module 2: Fetching and executing arguments
        switch(*traverse) 
        { 
            case 'd': {
				i = va_arg(arg, int);         //Fetch Decimal/Integer argument
				if(i < 0) 
				{ 
					i = -i;
					fputc('-', logfile); 
				} 
				fputs(convert(i,10), logfile);
				break;
			}
            case 's': {
				s = va_arg(arg, char *);       //Fetch string
				fputs(s, logfile); 
				break; 
			}
        }   
    }
	fflush(logfile);
	fclose(logfile);

    //Module 3: Closing argument list to necessary clean-up
    va_end(arg); 
}


// ----------------------------------- DEAMON -----------------------------------
int monitorProc() {
    pid_t pid;
    int need_start = 1;
    sigset_t sigset;
    siginfo_t siginfo;
	log_prefix = "[MONITOR]";

    // настраиваем сигналы которые будем обрабатывать
    sigemptyset(&sigset);
    
    // сигнал запроса завершения процесса
    sigaddset(&sigset, SIGTERM);
    
    // сигнал посылаемый при изменении статуса дочернего процесса
    sigaddset(&sigset, SIGCHLD);
    
    // пользовательский сигнал который мы будем использовать для обновления конфига
    sigaddset(&sigset, SIGHUP);
	sigprocmask(SIG_BLOCK, &sigset, NULL);

    // данная функция создаст файл с нашим PID'ом
    setPidFile(PID_FILE);

    for (;;) {
        if (need_start)
            pid = fork();
        
        need_start = 1;
        
        if (pid == -1) // если произошла ошибка
            writeLog("%s (%s): Fork failed (%s)\n", log_prefix, getTime(), strerror(errno));
        else if (!pid) {// запустим функцию отвечающую за работу демона
			sigprocmask(SIG_UNBLOCK, &sigset, NULL);
            status = gameMaster();
            exit(status);
        }
        else {
            // ожидаем поступление сигнала
            sigwaitinfo(&sigset, &siginfo);
            
            // если пришел сигнал от потомка
            if (siginfo.si_signo == SIGCHLD) {
                // получаем статус завершение
                wait(&status);
                
                // преобразуем статус в нормальный вид
                status = WEXITSTATUS(status);

                 // если потомок завершил работу с кодом говорящем о том, что нет нужды дальше работать
                if (status == CHILD_NEED_TERMINATE) {    
                    writeLog("%s (%s): Child stopped\n", log_prefix, getTime());
                    break;
                }
                else // если требуется перезапустить потомка
                    writeLog("%s (%s): Child restart\n", log_prefix, getTime());
            }
            else if (siginfo.si_signo == SIGHUP) {// если пришел сигнал что необходимо перезагрузить конфиг
				loadConfig(cfg_file);
                kill(pid, SIGHUP); // перешлем его потомку
                need_start = 0; // установим флаг что нам не надо запускать потомка заново
            }
            else {// если пришел сигнал звершения
                writeLog("%s (%s): Signal %s\n", log_prefix, getTime(), strsignal(siginfo.si_signo));

                kill(pid, SIGTERM);
				wait(NULL);
                status = 0;
                break;
            }
        }
    }

    writeLog("%s (%s): Stop\n", log_prefix, getTime());
    unlink(PID_FILE);
    
    return status;
}


// ----------------------------------- HANDLER ARBITRATOR -----------------------------------
void handlerArbitrator(int ret_status, char * mesg) {
	int client_socket = atoi(parse(mesg, "arb"));
	// 2 - считываем сокет клиента, выполняем sendNotification
	// 3 - считываем сокет клиента, сохраняем его, ка кпервого игрока, уведомляем всех о созданной игре (создаем ее), а игроку говорим ждать, считываем пароль и запоминаем в pwd
	// 4 - считываем сокет клиента, сохраняем его, как второго игрока, уведомляем всех о начале игры, отключаем всех, кроме игроков, игрокам выодим правила и слово,
	//	   крутим барабан, выбираем так игрока, который первый ходит 0 - второй, 1-9 - первый, меняем состояние в общей памяти на необходимое
	// 10 - считываем сокет клиента, пишем ему ожидать, пока завершит свой ход другой игрок
	// 11 - создаем executeGuess
	// 12 - создаем executeAnswer
	switch (ret_status) {
		case SEND_NOTIFICATION: {
			sendNotification(client_socket);
			break;
		}
		case CREATE_GAME: {
			sigset_t sigset;
			sigemptyset(&sigset);
			sigaddset(&sigset, SIGHUP);
			sigprocmask(SIG_BLOCK, &sigset, NULL);
			
			player_points[0] = 0;
			player_points[1] = 0;
			blocked_player = 0;
			
			player_sockets[0] = client_socket;
			cur_players = 1;
			
			strcpy(pwd, parse(mesg, "pwd"));
			for (int i = 0; i < clients_amount; ++i)
				sendNotification(client_socket_fd[i]);
			
			// генерируем стартовые условия
			if ((child_pid = fork()) == -1) {
				writeLog("%s (%s): create game error: fork: %s\n", log_prefix, getTime(), strerror(errno));
				player_sockets[0] = 0;
				cur_players = 0;
				bzero(pwd, 20);
				
				if ((attached_data = shmat(shmid, NULL, 0)) == (char *) -1) {
					writeLog("[ERROR] %s (%s): shmat: %s\n", log_prefix, getTime(), strerror(errno));
					break;
				}
					
				game_h = (struct gameStruct *) attached_data;
				semaphore = *(game_h->sem_lock);
				sem_wait(&semaphore);
			
				game_h->state = SEND_NOTIFICATION;

				sem_post(&semaphore);
				
				if (shmdt(attached_data) == -1)
					writeLog("[ERROR] %s (%s): shmdt: %s\n", log_prefix, getTime(), strerror(errno));
				break;
			}
			else if (child_pid == 0) //--то мы создаем копию нашего сервера для работы с другим клиентом(то есть один сеанс уде занят дублируем свободный процесс)
				exit(generator(client_socket));
			break;
		}
		case START_GAME: {
			player_sockets[1] = client_socket;
			cur_players++;
			for (int i = 0; i < clients_amount; ++i) {
				sendNotification(client_socket_fd[i]);
				if (client_socket_fd[i] != player_sockets[0] && client_socket_fd[i] != player_sockets[1])
					disconnect(i);
			}
			// крутим барабан и вычисляем первого игрока
			int first_player = 0, cur_state;
			char msg[512] = {0};
			if ((attached_data = shmat(shmid, NULL, 0)) == (char *) -1) {
				writeLog("[ERROR] %s (%s): shmat: %s\n", log_prefix, getTime(), strerror(errno));
				break;
			}
				
			game_h = (struct gameStruct *) attached_data;
			semaphore = *(game_h->sem_lock);
			while (!(cur_points = generateRandomByte() % MAX_POINT))
				first_player = (first_player + 1) % 2;
			
			if (!first_player) {
				cur_state = CLIENT1_TURN;
				sprintf(msg, "First player's turn is now\n He can get %d points for right guess\n\tWORD: ", cur_points);
			} else {
				cur_state = CLIENT2_TURN;
				sprintf(msg, "Second player's turn is now\n He can get %d points for right guess\n\tWORD: ", cur_points);
			}
			strncat(msg, game_h->cur_word, 255);
			strcat(msg, "\n");
			writeLog("%s (%s): chose, who play first - player %d\n", log_prefix, getTime(), first_player + 1);
			
			sem_wait(&semaphore);
			game_h->state = cur_state;
			sem_post(&semaphore);
			
			if (shmdt(attached_data) == -1) {
				writeLog("[ERROR] %s (%s): shmdt: %s\n", log_prefix, getTime(), strerror(errno));
				break;
			}
			
			for (int i = 0; i < clients_amount; ++i)
				send(client_socket_fd[i], msg, strlen(msg), 0);
			break;
		}
		case WRONG_TURN: {
			char * msg;
			msg = "wait for another player to make his turn\n";
			send(client_socket, msg, strlen(msg), 0);
			break;
		}
		case NEED_EXV_GUESS: {
			char * info = parse(mesg, "info");
			if ((child_pid = fork()) == -1) {
				writeLog("%s (%s): create executorGuess: fork: %s\n", log_prefix, getTime(), strerror(errno));
			} else if (child_pid == 0)
				exit(executeGuess(info, client_socket));
			break;
		}
		case NEED_EXV_ANSWER: {
			char * info = parse(mesg, "info");
			if ((child_pid = fork()) == -1) {
				writeLog("%s (%s): create executorAnswe: fork: %s\n", log_prefix, getTime(), strerror(errno));
			} else if (child_pid == 0)
				exit(executeAnswer(info, client_socket));
			break;
		}
		default:
			return;
	}
}


// ----------------------------------- HANDLER EXECUTOR GUESS -----------------------------------
int handlerExecutorGuess(int ret_status, char * mesg) {
	char msg[512] = {0};
	int client_socket = atoi(parse(mesg, "exg")), cur_player = 0, end_flag = 0;
	cur_points = atoi(parse(mesg, "points"));
	
	if ((attached_data = shmat(shmid, NULL, 0)) == (char *) -1) {
		writeLog("[ERROR] %s (%s): shmat: %s\n", log_prefix, getTime(), strerror(errno));
		return -1;
	}
		
	game_h = (struct gameStruct *) attached_data;
	semaphore = *(game_h->sem_lock);
	
	switch (ret_status) { // FIXME обработка блокированного игрока после неверного answer
		case CLIENT1_TURN: {
			if (cur_points) {
				player_points[0] += cur_points;
				strcpy(msg, "First player guessed the letter.\n");
				writeLog("%s (%s): First player gave right letter - player's scores: %d %d\n", 
							log_prefix, getTime(), player_points[0], player_points[1]);
			} else if (blocked_player) {
				strcpy(msg, "First player did not guess the letter. But he can try again\n");
				writeLog("%s (%s): First player gave wrong letter - player's scores: %d %d\n", 
							log_prefix, getTime(), player_points[0], player_points[1]);
			} else {
				strcpy(msg, "Second player did not guess the letter.\n");
				writeLog("%s (%s): Second player gave wrong letter - player's scores: %d %d\n", 
							log_prefix, getTime(), player_points[0], player_points[1]);
			}
			cur_player = 0;
			break;
		}
		case CLIENT2_TURN: {
			if (cur_points) {
				player_points[1] += cur_points;
				strcpy(msg, "Second player guessed the letter.\n");
				writeLog("%s (%s): Second player gave right letter - player's scores: %d %d\n", 
							log_prefix, getTime(), player_points[0], player_points[1]);
			} else if (blocked_player) {
				strcpy(msg, "Second player did not guess the letter. But he can try again\n");
				writeLog("%s (%s): Second player gave wrong letter - player's scores: %d %d\n", 
							log_prefix, getTime(), player_points[0], player_points[1]);
			} else {
				strcpy(msg, "First player did not guess the letter.\n");
				writeLog("%s (%s): First player gave wrong letter - player's scores: %d %d\n", 
							log_prefix, getTime(), player_points[0], player_points[1]);
			}
			cur_player = 1;
			break;
		}
		case WINNER1: {
			end_flag = 1;
			player_points[0] += cur_points;
			writeLog("%s (%s): First player win - player's scores: %d %d\n", 
					log_prefix, getTime(), player_points[0], player_points[1]);
			sprintf(msg, "First player win - player's scores: %d %d\n\tWORD: %s\n",
					player_points[0], player_points[1], game_h->cur_word);
			// Расформировываем игру
			ret_status = SEND_NOTIFICATION;
			sigset_t sigset;
			sigemptyset(&sigset);
			sigaddset(&sigset, SIGHUP);
			sigprocmask(SIG_UNBLOCK, &sigset, NULL);
			cur_players = 0;
			break;
		}
		case WINNER2: {
			end_flag = 1;
			player_points[1] += cur_points;
			writeLog("%s (%s): Second player win - player's scores: %d %d\n", 
					log_prefix, getTime(), player_points[0], player_points[1]);
			sprintf(msg, "Second player win - player's scores: %d %d\n\tWORD: %s\n",
					player_points[0], player_points[1], game_h->cur_word);
			// Расформировываем игру
			ret_status = SEND_NOTIFICATION;
			sigset_t sigset;
			sigemptyset(&sigset);
			sigaddset(&sigset, SIGHUP);
			sigprocmask(SIG_UNBLOCK, &sigset, NULL);
			cur_players = 0;
			break;
		}
		default:
			return -1;
	}
	
	if (!end_flag) {
		while ((cur_points = generateRandomByte() % MAX_POINT) == 0)
			cur_player = (cur_player + 1 + (blocked_player >= 0)) % 2; // обработка, кто след ходит. Если есть заблоченный игрок, то ход переходит к тому же игроку
		
		if (!cur_player) {
			strcat(msg, "First player's turn is now\n He can get ");
			strcat(msg, convert(cur_points, 10));
			ret_status = CLIENT1_TURN;
		}
		else {
			strcat(msg, "Second player's turn is now\n He can get ");
			strcat(msg, convert(cur_points, 10));
			ret_status = CLIENT2_TURN;
		}
		
		strcat(msg, " points\n\tWORD: ");
		
		strncat(msg, game_h->cur_word, 255);
		writeLog("%s (%s): word - %s\n\tcurrent word progress - %s\n", log_prefix, getTime(), game_h->word, game_h->cur_word);
		strcat(msg, "\n");
	}
	
	// работа с shared memory
	sem_wait(&semaphore);
	game_h->state = ret_status;
	sem_post(&semaphore);
	// конец работы с shared memory
	
	if (shmdt(attached_data) == -1) {
		writeLog("[ERROR] %s (%s): shmdt: %s\n", log_prefix, getTime(), strerror(errno));
		return -1;
	}
	
	(void)client_socket;
	for (int i = 0; i < clients_amount; ++i)
		send(client_socket_fd[i], msg, strlen(msg), 0);
	return 0;
}


// ----------------------------------- HANDLER EXECUTOR ANSWER -----------------------------------
int handlerExecutorAnswer(int ret_status, char * mesg) {
	char msg[512] = {0};
	int client_socket = atoi(parse(mesg, "exa")), end_flag = 0;
	cur_points = atoi(parse(mesg, "points"));
	
	if ((attached_data = shmat(shmid, NULL, 0)) == (char *) -1) {
		writeLog("[ERROR] %s (%s): shmat: %s\n", log_prefix, getTime(), strerror(errno));
		return -1;
	}
		
	game_h = (struct gameStruct *) attached_data;
	semaphore = *(game_h->sem_lock);
	
	switch (ret_status) {
		case CLIENT1_TURN: {
			strcpy(msg, "Second player did not guess the word.\n");
			writeLog("%s (%s): Second player gave wrong word - player's scores: %d %d\n", 
						log_prefix, getTime(), player_points[0], player_points[1]);
			break;
		}
		case CLIENT2_TURN: {
			strcpy(msg, "First player did not guess the word.\n");
			writeLog("%s (%s): First player gave wrong word - player's scores: %d %d\n", 
						log_prefix, getTime(), player_points[0], player_points[1]);
			break;
		}
		case WINNER1: {
			end_flag = 1;
			player_points[0] += cur_points;
			writeLog("%s (%s): First player win - player's scores: %d %d\n", 
					log_prefix, getTime(), player_points[0], player_points[1]);
			sprintf(msg, "First player win - player's scores: %d %d\n\tWORD: %s\n",
					player_points[0], player_points[1], game_h->cur_word);
			// Расформировываем игру
			ret_status = SEND_NOTIFICATION;
			sigset_t sigset;
			sigemptyset(&sigset);
			sigaddset(&sigset, SIGHUP);
			sigprocmask(SIG_UNBLOCK, &sigset, NULL);
			cur_players = 0;
			break;
		}
		case WINNER2: {
			end_flag = 1;
			player_points[1] += cur_points;
			writeLog("%s (%s): Second player win - player's scores: %d %d\n", 
					log_prefix, getTime(), player_points[0], player_points[1]);
			sprintf(msg, "Second player win - player's scores: %d %d\n\tWORD: %s\n",
					player_points[0], player_points[1], game_h->cur_word);
			// Расформировываем игру
			ret_status = SEND_NOTIFICATION;
			sigset_t sigset;
			sigemptyset(&sigset);
			sigaddset(&sigset, SIGHUP);
			sigprocmask(SIG_UNBLOCK, &sigset, NULL);
			cur_players = 0;
			break;
		}
		default:
			return -1;
	}
	
	if (!end_flag) {
		if (blocked_player) {
			// расформировали игру
			ret_status = SEND_NOTIFICATION;
			sigset_t sigset;
			sigemptyset(&sigset);
			sigaddset(&sigset, SIGHUP);
			sigprocmask(SIG_UNBLOCK, &sigset, NULL);
			cur_players = 0;
			strcat(msg, "Everyone lost the game\n\tWORD: ");
			strncat(msg, game_h->word, 255);
			strcat(msg, "\n");
		} else {
			blocked_player = client_socket;
			while ((cur_points = generateRandomByte() % MAX_POINT) == 0);
			
			if (ret_status == CLIENT1_TURN) {
				strcat(msg, "First player's turn is now\n He can get ");
				strcat(msg, convert(cur_points, 10));
			}
			else {
				strcat(msg, "Second player's turn is now\n He can get ");
				strcat(msg, convert(cur_points, 10));
			}
			
			strcat(msg, " points\n\tWORD: ");
			strncat(msg, game_h->cur_word, 255);
			writeLog("%s (%s): word - %s\n\tcurrent word progress - %s\n", log_prefix, getTime(), game_h->word, game_h->cur_word);
			strcat(msg, "\n");
		}
	}
	
	// работа с shared memory
	sem_wait(&semaphore);
	game_h->state = ret_status;
	sem_post(&semaphore);
	// конец работы с shared memory
	
	if (shmdt(attached_data) == -1) {
		writeLog("[ERROR] %s (%s): shmdt: %s\n", log_prefix, getTime(), strerror(errno));
		return -1;
	}
	
	for (int i = 0; i < clients_amount; ++i)
		send(client_socket_fd[i], msg, strlen(msg), 0);
	return 0;
}


// ----------------------------------- GAME MASTER -----------------------------------
void handlerSigChld(int sig) {
	(void)sig;
	pid_t type = wait(&status);
	int ret_status = WEXITSTATUS(status);
	child_pid = -1;
	writeLog("%s (%s): get retcode %d from child %d\n", log_prefix, getTime(), ret_status, type);
	
	if (ret_status == -1)
		return;
	
	if(msgrcv(msqid, &msg_h, MSGSZ, type, MSG_NOERROR) <= 0)
		return;	
	
	if (!strncmp(msg_h.mesg, "arb ", 4))
		handlerArbitrator(ret_status, msg_h.mesg);
	else if (!strncmp(msg_h.mesg, "gen ", 4)) {
		int client_socket = atoi(parse(msg_h.mesg, "gen"));
		// 3 - считываем сокет игрока, пишем ему, что игра создана
		char * msg;
		msg = "Your game has been created\n";
		send(client_socket, msg, strlen(msg), 0);
	}
	else if (!strncmp(msg_h.mesg, "exg ", 4))
		handlerExecutorGuess(ret_status, msg_h.mesg);
	else if (!strncmp(msg_h.mesg, "exa ", 4))
		handlerExecutorAnswer(ret_status, msg_h.mesg);	

}


// закрытие активных соединений, удаление очереди сообщений, очистка общей struct, ожидание дорабатывающих child
void handlerSigTerm(int sig) {
	(void)sig;
	if (-1 != child_pid)
		wait(&status);
	need_terminate = 1;
	
	close(socket_fd);
	for (int i = 0; i < clients_amount; ++i)
		close(client_socket_fd[i]);
	
	if (msqid != -1)
		msgctl(msqid, IPC_RMID, NULL);
    sem_destroy(&semaphore);
	if (shmid != -1)
		shmctl(shmid, IPC_RMID, NULL);
	writeLog("%s (%s): Terminated\n", log_prefix, getTime());
}


void handlerSigHup(int sig) {
	(void)sig;
	status = reloadConfig(cfg_file);
	if (status == -1)
		writeLog("%s (%s): Reload config failed\n", log_prefix, getTime());
	else
		writeLog("%s (%s): Reload config OK\n", log_prefix, getTime());
}


void installSignalHandlers() {
	struct sigaction act;
	act.sa_handler = handlerSigTerm;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGHUP);
	sigaddset(&act.sa_mask, SIGCHLD);
	act.sa_flags = SA_ONESHOT;
	
	sigaction(SIGTERM, &act, NULL);
	signal(SIGCLD, handlerSigChld);
	signal(SIGHUP, handlerSigHup);
}


int createMsgQueue() {
	if ((msqid = msgget(IPC_PRIVATE, S_IRWXU | S_IRWXG | S_IRWXO)) == -1) {
		writeLog("[ERROR] %s (%s): msg queue create: %s\n", log_prefix, getTime(), strerror(errno));
		return -1;
	}
	return 0;
}


int createSharedMemory() {
    if ((shmid = shmget(IPC_PRIVATE, sizeof(char) * 512 + sizeof(int) + sizeof(sem_t *), 0644)) == -1) {
		writeLog("[ERROR] %s (%s): shmget: %s\n", log_prefix, getTime(), strerror(errno));
        return -1;
    }
	
	if ((attached_data = shmat(shmid, NULL, 0)) == (char *) -1) {
		writeLog("[ERROR] %s (%s): shmat: %s\n", log_prefix, getTime(), strerror(errno));
        return -1;
    }
		
	game_h = (struct gameStruct *) attached_data;
	game_h->state = SEND_NOTIFICATION;
	*(game_h->word) = '\0';
	*(game_h->cur_word) = '\0';
	game_h->sem_lock = &semaphore;
	
    if (shmdt(attached_data) == -1) {
		writeLog("[ERROR] %s (%s): shmdt: %s\n", log_prefix, getTime(), strerror(errno));
        return -1;
    }
	
	return 0;
}


int arbitrator(char * client_msg) {
	int cur_state, ret_state;
	char * got_info;
	log_prefix = "[ARBITRATOR]";
	writeLog("%s pid %d (%s): start work with msg %s\n", log_prefix, getpid(), getTime(), client_msg);
	// cur_socket - перед созданием процесса, мастер поместил сокет в эту переменную
	
	msg_h.mtype = getpid();
	bzero(msg_h.mesg, MSGSZ);
	strcpy(msg_h.mesg, "arb ");
	strcat(msg_h.mesg, convert(cur_socket, 10));
	
	// проверка сбщ клиента
	// чтение с shared memory
	if ((attached_data = shmat(shmid, NULL, 0)) == (char *) -1) {
		writeLog("[ERROR] %s (%s): shmat: %s\n", log_prefix, getTime(), strerror(errno));
        return -1;
    }
		
	game_h = (struct gameStruct *) attached_data;
	semaphore = *(game_h->sem_lock);
	cur_state = game_h->state;
	writeLog("%s pid %d (%s): current state %d\n", log_prefix, getpid(), getTime(), cur_state);
	// -------------------------проверяем, какой состояние
	if (cur_state == SEND_NOTIFICATION) { // изначальное состояние, ждем сообщения start{pwd}
		got_info = parse(client_msg, "start");
		if (strlen(got_info) == 0)
			ret_state = SEND_NOTIFICATION;
		else {
			strcat(msg_h.mesg, " pwd "); // мастер должен запомнить пароль
			strcat(msg_h.mesg, got_info);
			cur_state = CREATE_GAME; // переводим в это состояние
			ret_state = CREATE_GAME; // мастер должен оповестить всех клиентов, что создана игра, запомнить этого клиента
		}
	} else if (cur_state == CREATE_GAME) {// ждем сбщ join{pwd}
		got_info = parse(client_msg, "join");
		if (strlen(got_info) == 0 || strncmp(pwd, got_info, strlen(got_info)))
			ret_state = SEND_NOTIFICATION;
		else {
			cur_state = START_GAME; // переводим в состояние начала игры
			ret_state = START_GAME; // мастер должен оповестить всех клиентов, что началась игра, запомнить этого клиента, отключить остальных
									// сформировать, кто первый ходит, оповестить об этом игроков, изменить значение в shared memory
		}
	} else if (cur_state == CLIENT1_TURN || cur_state == CLIENT2_TURN) {
		writeLog("%s pid %d (%s): current socket %d\nPlayers sockets %d %d\n", log_prefix, getpid(), getTime(), cur_socket, player_sockets[0], player_sockets[1]);
		// после появления второго игрока, переходим в это состояние (переход должен выполнить EXECUTOR, в мастере обновляем текущее кол-во игроков)
		// если мастеру приходит данное состояние, значит сбщ пользователя прошло проверку и требуется запустить EXECUTOR
		if ((cur_socket != player_sockets[0] && cur_state == CLIENT1_TURN) || (cur_socket != player_sockets[1] && cur_state == CLIENT2_TURN)) {
			ret_state = WRONG_TURN;
		}
		else {
			got_info = parse(client_msg, "guess");
			if (strlen(got_info) == 1) {
				strcat(msg_h.mesg, " info "); // мастер должен отправить данные исполнителю
				strcat(msg_h.mesg, got_info);
				ret_state = NEED_EXV_GUESS;
			}
			else {
				got_info = parse(client_msg, "answer");
				if (strlen(got_info) == 0)
					ret_state = SEND_NOTIFICATION;
				else {
					strcat(msg_h.mesg, " info "); // мастер должен отправить данные исполнителю
					strcat(msg_h.mesg, got_info);
					ret_state = NEED_EXV_ANSWER;
				}
			}
		}
	} else if (cur_state == GAME_END)// расформирование игры
		ret_state = SEND_NOTIFICATION;
	else
		ret_state = SEND_NOTIFICATION;
	
	// работа с shared memory
	sem_wait(&semaphore);
	
	if (game_h->state == cur_state && (cur_state == START_GAME || cur_state == CREATE_GAME))// обработка, что не пришло одновременно несколько одинаковых запросов
		ret_state = SEND_NOTIFICATION;														// например, два start
	else
		game_h->state = cur_state;

	sem_post(&semaphore);
	
    if (shmdt(attached_data) == -1) {
		writeLog("[ERROR] %s (%s): shmdt: %s\n", log_prefix, getTime(), strerror(errno));
        return -1;
    }
	// конец работы с shared memory
	// FIXME msq - "arb socket" с помощью retcode возвращаем состояние, говорящее, как ответить
	
	if (msgsnd(msqid, &msg_h, sizeof(msg_h.mesg), IPC_NOWAIT)) {
		writeLog("[ERROR] %s (%s): msgsnd: %s\n", log_prefix, getTime(), strerror(errno));
		return -1;
	}
	writeLog("%s pid %d (%s): send to master %s\n", log_prefix, getpid(), getTime(), msg_h.mesg);
	return ret_state;
}


int executeAnswer(char * client_word, int client_socket) {
	log_prefix = "[EXECUTOR ANSWER]";
	int state = 0;
	char word[256] = {0}, cur_word[256] = {0};
	msg_h.mtype = getpid();
	bzero(msg_h.mesg, MSGSZ);
	strcpy(msg_h.mesg, "exa ");
	strcat(msg_h.mesg, convert(client_socket, 10));
	strcat(msg_h.mesg, " points ");
	writeLog("%s pid %d (%s): client %d start work with %s\n", log_prefix, getpid(), getTime(), client_socket, client_word);
	
	// работа с shared memory
	if ((attached_data = shmat(shmid, NULL, 0)) == (char *) -1) {
		writeLog("[ERROR] %s (%s): shmat: %s\n", log_prefix, getTime(), strerror(errno));
        return -1;
    }
	
	game_h = (struct gameStruct *) attached_data;
	semaphore = *(game_h->sem_lock);
	state = game_h->state;
	strncpy(word, game_h->word, 255);
	strncpy(cur_word, game_h->cur_word, 255);
	int word_len = (int)strlen(word);
	
	// проверяем, есть ли буква в слове
	if (!strncmp(word, client_word, word_len)) {
		strncpy(cur_word, word, 255);
		strcat(msg_h.mesg, convert(cur_points, 10));
		if (state == CLIENT1_TURN)
			state = WINNER1;
		else if (state == CLIENT2_TURN)
			state = WINNER2;
	}
	else {
		strcat(msg_h.mesg, "0");
		if (state == CLIENT1_TURN)
			state = CLIENT2_TURN;
		else if (state == CLIENT2_TURN)
			state = CLIENT1_TURN;
	}
	
	// работа с shared memory
	sem_wait(&semaphore);
	game_h->state = state;
	strncpy(game_h->cur_word, cur_word, 255);
	sem_post(&semaphore);
	// конец работы с shared memory
	
    if (shmdt(attached_data) == -1) {
		writeLog("[ERROR] %s (%s): shmdt: %s\n", log_prefix, getTime(), strerror(errno));
        return -1;
    }
	// конец работы с shared memory
	if (msgsnd(msqid, &msg_h, sizeof(msg_h.mesg), IPC_NOWAIT)) {
		writeLog("[ERROR] %s (%s): msgsnd: %s\n", log_prefix, getTime(), strerror(errno));
		return -1;
	}
	writeLog("%s pid %d (%s): send to master %s\n", log_prefix, getpid(), getTime(), msg_h.mesg);
	return state;
}


int executeGuess(char * letter, int client_socket) {
	log_prefix = "[EXECUTOR GUESS]";
	int state = 0;
	char word[256] = {0}, cur_word[256] = {0};
	msg_h.mtype = getpid();
	bzero(msg_h.mesg, MSGSZ);
	strcpy(msg_h.mesg, "exg ");
	strcat(msg_h.mesg, convert(client_socket, 10));
	strcat(msg_h.mesg, " points ");
	writeLog("%s pid %d (%s): client %d start work with %s\n", log_prefix, getpid(), getTime(), client_socket, letter);
	
	// работа с shared memory
	if ((attached_data = shmat(shmid, NULL, 0)) == (char *) -1) {
		writeLog("[ERROR] %s (%s): shmat: %s\n", log_prefix, getTime(), strerror(errno));
        return -1;
    }
	
	game_h = (struct gameStruct *) attached_data;
	semaphore = *(game_h->sem_lock);
	state = game_h->state;
	strncpy(word, game_h->word, 255);
	strncpy(cur_word, game_h->cur_word, 255);
	int word_len = (int)strlen(word), flag = 0, end_flag = 1;
	
	// проверяем, есть ли буква в слове
	for (int i = 0; i < word_len; ++i)
		if (word[i] == *letter) {
			flag = 1;
			cur_word[i] = *letter;
		}
	
	for (int i = 0; i < word_len; ++i)
		if (cur_word[i] == '*')
			end_flag = 0;
	
	if (end_flag) {
		strcat(msg_h.mesg, convert(cur_points, 10));
		if (state == CLIENT1_TURN)
			state = WINNER1;
		else if (state == CLIENT2_TURN)
			state = WINNER2;
	} else if (!flag) { // FIXME обработка блокированного игрока после неверного answer
		if (state == CLIENT1_TURN && !blocked_player)
			state = CLIENT2_TURN;
		else if (state == CLIENT2_TURN && !blocked_player)
			state = CLIENT1_TURN;
		strcat(msg_h.mesg, "0");
	} else strcat(msg_h.mesg, convert(cur_points, 10));
	
	// работа с shared memory
	sem_wait(&semaphore);
	game_h->state = state;
	strncpy(game_h->cur_word, cur_word, 255);
	sem_post(&semaphore);
	// конец работы с shared memory
	
    if (shmdt(attached_data) == -1) {
		writeLog("[ERROR] %s (%s): shmdt: %s\n", log_prefix, getTime(), strerror(errno));
        return -1;
    }
	// конец работы с shared memory
	if (msgsnd(msqid, &msg_h, sizeof(msg_h.mesg), IPC_NOWAIT)) {
		writeLog("[ERROR] %s (%s): msgsnd: %s\n", log_prefix, getTime(), strerror(errno));
		return -1;
	}
	writeLog("%s pid %d (%s): send to master %s\n", log_prefix, getpid(), getTime(), msg_h.mesg);
	return state;
}


unsigned int generateRandomByte() {
	// получение в консоли такое: od -A n -t d -N 1 /dev/urandom | tr -d ' '
	// получаем данные напрямую
	int random_data = open("/dev/urandom", O_RDONLY);
	if (random_data < 0) {
		writeLog("%s (%s) ERROR: getting random number failed: %s\n", log_prefix, getTime(), strerror(errno));
		return -1;
	}
	
	char my_random_data[2];
	ssize_t result = read(random_data, my_random_data, sizeof(my_random_data));
	if (result < 0) {
		writeLog("%s (%s) ERROR: getting random number failed: %s\n", log_prefix, getTime(), strerror(errno));
		return -1;
	}
	
	return (unsigned int)my_random_data[0];
}


int generator(int client_socket) {
	// генерирует слово, которое надо отгадать
	// cur_socket - перед созданием процесса, мастер поместил сокет в эту переменную
	log_prefix = "[GENERATOR]";
	
	FILE *f;
    size_t lineno = 0, line_num = (size_t)generateRandomByte() % 100; // выбирается случайное число - номер строки
    size_t selectlen;
	char selected[256] = {0}; // здесь хранится выбранная строка
    char current[256];

    f = fopen("word_list.txt", "r"); // FIXME добавить файл со словами для игры в конфиг
	if (f == NULL) {
		// заглушка 
		strcpy(selected, "miracle");
		selectlen = strlen(selected) + 1;
	}
	else {
		while (fgets(current, sizeof(current), f)) {
			if (line_num == lineno) {
				strcpy(selected, current);
				break;
			}
			++lineno;
		}
		fclose(f);
		selectlen = strlen(selected);
		if (selectlen > 0 && selected[selectlen-1] == '\n') {
			selected[selectlen-1] = '\0';
		}
	}
	writeLog("%s (%s): selected string = %s\n", log_prefix, getTime(), selected);
	// FIXME добавить сохранение выбранного слова в shared memory
	
	
	if ((attached_data = shmat(shmid, NULL, 0)) == (char *) -1) {
		writeLog("[ERROR] %s (%s): shmat: %s\n", log_prefix, getTime(), strerror(errno));
        return -1;
    }
		
	game_h = (struct gameStruct *) attached_data;
	semaphore = *(game_h->sem_lock);
	
	// работа с shared memory
	sem_wait(&semaphore);
	bzero(game_h->word, 256);
	bzero(game_h->cur_word, 256);
	strncpy(game_h->word, selected, selectlen);
	memset(game_h->cur_word, '*', strlen(selected));
	*(game_h->cur_word + selectlen) = '\0';

	sem_post(&semaphore);
	// конец работы с shared memory
	
    if (shmdt(attached_data) == -1) {
		writeLog("[ERROR] %s (%s): shmdt: %s\n", log_prefix, getTime(), strerror(errno));
        return -1;
    }
	// добавить сбщ в msq "gen player_socket" - передаем, что мы создали игру по инициативе клиента с сокетом player_socket
	msg_h.mtype = getpid();
	bzero(msg_h.mesg, MSGSZ);
	strcpy(msg_h.mesg, "gen ");
	strcat(msg_h.mesg, convert(client_socket, 10));
	
	if (msgsnd(msqid, &msg_h, sizeof(msg_h.mesg), IPC_NOWAIT)) {
		writeLog("[ERROR] %s (%s): msgsnd: %s\n", log_prefix, getTime(), strerror(errno));
		return -1;
	}
	writeLog("%s pid %d (%s): send to master %s\n", log_prefix, getpid(), getTime(), msg_h.mesg);
	
    return CREATE_GAME;
}


int sendNotification(int socket_fd) {
	char * msg;
	int ret = 0;
	
	if (cur_players == MAX_PLAYERS) {
		if (socket_fd == player_sockets[0] || socket_fd == player_sockets[1])
			msg = "game: server tell how many scores you will get for right letter\n\tYou can send to server: guess {letter} or answer {word}\n";
		else {
			msg = "Now a game is going. Please, reconnect later\n";
			ret = 1;
		}
	}
	else if (cur_players == MAX_PLAYERS - 1)
		if (socket_fd == player_sockets[0])
			msg = "Wait for the second player to connect tha game\n";
		else
			msg = "There is a game: you can join to it\n";
	else 
		msg = "There is no game: you can create it\n";
	
	send(socket_fd, msg, strlen(msg), 0);
	return ret;
}


int disconnect(int index) {
	if ((attached_data = shmat(shmid, NULL, 0)) == (char *) -1) {
		writeLog("[ERROR] %s (%s): shmat: %s\n", log_prefix, getTime(), strerror(errno));
		return -1;
	}
		
	game_h = (struct gameStruct *) attached_data;
	semaphore = *(game_h->sem_lock);
	
	if (game_h->state != SEND_NOTIFICATION && (cur_socket == player_sockets[0] || cur_socket == player_sockets[1])) {
		// Расформировываем игру
		sigset_t sigset;
		sigemptyset(&sigset);
		sigaddset(&sigset, SIGHUP);
		sigprocmask(SIG_UNBLOCK, &sigset, NULL);
		cur_players = 0;
		// работа с shared memory
		sem_wait(&semaphore);
		game_h->state = SEND_NOTIFICATION;
		sem_post(&semaphore);
		// конец работы с shared memory
	}
	
	if (shmdt(attached_data) == -1) {
		writeLog("[ERROR] %s (%s): shmdt: %s\n", log_prefix, getTime(), strerror(errno));
		return -1;
	}
	
	if (!((index == 0 && clients_amount == 1) || index == (clients_amount - 1)))
		client_socket_fd[index] = client_socket_fd[clients_amount - 1];
	client_socket_fd[clients_amount - 1] = 0;

	clients_amount--;
	
	return close(cur_socket);
}


int checkSockets() {
	int nbytes, ret;
	struct pollfd pfd;
	// обработка соединений на входящие сообщения
	for (int i = 0; i < clients_amount; ++i) {
		cur_socket = client_socket_fd[i];
		// FIXME если это текущий игрок, ждущий в сессии, то сбщ ОЖИДАЙТЕ
		// если есть оба игрока, то отсоединить остальных
		
		pfd.fd = cur_socket;
		pfd.revents = 0;
		pfd.events = POLLIN | POLLHUP;
		ret = poll(&pfd, 1, 100);

		if (ret < 0) {
			if (EINTR == errno)
				continue;
			disconnect(i);
			writeLog("%s (%s): some error with connection %d: %s\n", log_prefix, getTime(), cur_socket, strerror(errno));
			break;
		}
		else if (ret == 0)
			continue;
		// получаем сбщ и создаем arbitrator
		bzero(sockbuff, 256); //--чистим наш буфер от всякого мусора
		nbytes = recv(cur_socket, sockbuff, 256, 0); //--читаем из в буфер из клиентского сокета
		strncat(sockbuff, "\0", 1); //--незабываем символ конца строки
		
		if (nbytes <= 0){
			disconnect(i);
			writeLog("%s (%s): client %d close connection\n", log_prefix, getTime(), cur_socket);
			break;
		}
		
		writeLog("%s (%s): Client %d send to Server %d bytes:%s\n", log_prefix, getTime(), cur_socket, nbytes, sockbuff);
		
		if ((child_pid = fork()) == -1) { // производим обработку сообщения
			writeLog("%s (%s): answer client error: fork\n", log_prefix, getTime());
		}
		else if (child_pid == 0) //--то мы создаем копию нашего сервера для работы с другим клиентом(то есть один сеанс уде занят дублируем свободный процесс)
			exit(arbitrator(sockbuff));
		// Возможна отправка каких-то данных по msg queue
		
	}
	
	return 0;
}


int gameMaster() {
	log_prefix = "[GAMEMASTER]";
	int new_socket;
	installSignalHandlers();
    // пользовательский сигнал который мы будем использовать для обновления конфига
    sigset_t sigset;
    sigemptyset(&sigset);
    sigaddset(&sigset, SIGHUP);
    
	if (connectSocket() == -1) // подключаем сервер к сокету
		return CHILD_NEED_WORK;
	
	if (createMsgQueue() == -1) {
		handlerSigTerm(SIGTERM);
		return CHILD_NEED_WORK;
	}
	
    sem_init(&semaphore, 1, 1);

	if (createSharedMemory() == -1) {
		handlerSigTerm(SIGTERM);
		return CHILD_NEED_WORK;
	}
	
	// FIXME добавить создание shared memory и semaphore (она должна хранить в heap)

    // запишем в лог, что наш демон стартовал
    writeLog("%s (%s): Started\n", log_prefix, getTime());
	
    for(;;) {//--бесконечный цикл
		if (need_terminate)
			break;
		
		new_socket = accept(socket_fd, (struct sockaddr *)&address, &size); //--подключение нашего клиента
		
		if (new_socket > 0) {//--если подключение прошло успешно ОПОВЕЩАНИЕ
			if (socketInNonblock(new_socket)) {
				writeLog("%s (%s): cannot change socket mode to nonblock mode\n", log_prefix, getTime());
				close(new_socket);
			}
			else {
				if (sendNotification(new_socket)) {
					writeLog("%s (%s): disconnect new client because of the game\n", log_prefix, getTime());
					close(new_socket);
				}
				else {
					writeLog("%s (%s): connect new client\n", log_prefix, getTime());
					client_socket_fd[clients_amount] = new_socket;
					clients_amount++;
				}
			}
		}
		checkSockets();
		sleep(1);
		
	}

    writeLog("%s (%s): Stopped\n", log_prefix, getTime());
    
    return CHILD_NEED_TERMINATE;
}


