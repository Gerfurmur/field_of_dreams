#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/poll.h>

int sock = 0;


void handler(int sig) {
	(void)sig;
	close(sock);
	kill(getpid(), SIGKILL);
}

   
int main(int argc, char const *argv[]){
	if (argc != 3) {
		fprintf(stderr, "Usage: ./%s <ipv4> <port>", argv[0]);
		return -1;
	}
	
	signal(SIGINT, handler);
	signal(SIGTERM, handler);
	
	int PORT = atoi(argv[2]);
	const char * ip4 = argv[1];
	int nbytes;
    struct sockaddr_in serv_addr;
	struct stat instat;
    char myMsg[1024] = {0};
    char buffer[1024] = {0};
	fstat(0, &instat);

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
	}
	
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // Преобразование адресов IPv4 и IPv6 из текста в двоичную форму
    if(inet_pton(AF_INET, ip4, &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
	struct pollfd pfd;
	int ret = 0;
	struct stat file_stat;

	if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
		printf("\nConnection Failed \n");
	}
	pfd.fd = sock;
	pfd.revents = 0;
	pfd.events = POLLIN | POLLHUP;
		
	do {
		bzero(myMsg, 1024);
		while (strlen(myMsg) == 0) {
			ret = poll(&pfd, 1, 100);

			if (ret < 0) {
				if (EINTR == errno)
					continue;
				close(sock);
				perror("some error with connection:");
				break;
			}
			else if (ret > 0) {
				bzero(buffer, 1024);
				if ((nbytes = recv(sock, buffer, 1024, MSG_NOSIGNAL)) > 0)
					printf("Server: %s\n", buffer);
			}
			else 
				gets(myMsg);
		}
		printf("Send to server: %s\n", myMsg);
		send(sock, myMsg, strlen(myMsg), 0);
		sleep(1);
	}
	while (nbytes > 0 && strncmp("disconnect", myMsg, 10));

	close(sock);

    return 0;

}